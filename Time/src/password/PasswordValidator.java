package password;

public class PasswordValidator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public static boolean isValid(String password) {
		
		int len = password.length();//length of password
		if (len < 8 ) {
			System.out.println("Invalid Password! Password length too short");
			return false;
		}else {
			System.out.println("Password is valid!!");
			return true;
		}
		
	}
	
	public static boolean checkDigits(String password) {
		int count = 0; //counts digits
		
		for(int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i))) {
				count++;
			}
		}
		if (count < 2) {
			return false;
		}else {
			return true;
		}
	}

}
