package time;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TimeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetMilliSeconds() {
		int milli = Time.getMilliSeconds("12:05:05:05");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly ", milli == 5);
	}
	
	@Test( expected = NumberFormatException.class)
	public void testGetMilliSecondsException() {
		int milli = Time.getMilliSeconds("12:05:05:0054");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly ", milli == 5);
	}
	
	@Test( expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundaryOut() {
		int result = Time.getMilliSeconds("12:05:05:1000");
		assertTrue("The Milliseconds were not calculated properly ", result == 0);
	}
	
	@Test
	public void testGetMilliSecondsBoundaryIn() {
		//Time timeTest = new Time();
		int milli = Time.getMilliSeconds("12:05:05:999");
		System.out.println(milli);
		assertTrue("The Milliseconds were not calculated properly ", milli == 999);
	}
	
//	@Test
//	public void testGetTotalSeconds() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetSeconds() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetTotalMinutes() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetTotalHours() {
//		fail("Not yet implemented");
//	}

}
