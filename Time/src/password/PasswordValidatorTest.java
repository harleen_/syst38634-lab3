package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void checkDigits() {
		assertTrue("The password doesn't fullfil needs ", PasswordValidator.checkDigits("helloo12") == true);
	}
	
	@Test
	public void checkDigitsException() {
		assertFalse("The password doesn't fullfil needs ", PasswordValidator.checkDigits("hellor") == true);
	}
	
	@Test
	public void checkDigitsBoundaryIn() {
		assertTrue("The password doesn't fullfil needs ", PasswordValidator.checkDigits("harlee12") == true);
	}
	
	@Test
	public void checkDigitsBoundaryOut() {
		assertFalse("The password doesn't fullfil needs ", PasswordValidator.checkDigits("harleen1") == true);
	}
	

	@Test
	public void checkPasswordLength() {
		assertTrue("The password doesn't fullfil needs ", PasswordValidator.isValid("hello123") == true);
	}

	@Test
	public void checkPasswordLengthException() {
		assertFalse("The password doesn't fullfil needs ", PasswordValidator.isValid("hellor") == true);
	}
	
	@Test
	public void checkPasswordLengthBoundaryIn() {
		assertTrue("The password doesn't fullfil needs ", PasswordValidator.isValid("har12345") == true);
	}
	
	@Test
	public void checkPasswordBoundaryOut() {
		assertFalse("The password doesn't fullfil needs ", PasswordValidator.isValid("harlee1") == true);
	}


}
